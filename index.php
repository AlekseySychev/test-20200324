<?php
require __DIR__ . '/vendor/autoload.php';

$start = microtime(true);
$template = new AlekseySychev\Template();

$template->setVar('my_var', "test");
$template->setVar('bool_var', true);

$template->setVar('var1', 1);
$template->setVar('var2', 2);

$template->setTemplate(__DIR__ . '/index.tpl');
$template->show();
$stop = microtime(true);


echo '<pre>' .($stop-$start) . ' sec</pre>';
