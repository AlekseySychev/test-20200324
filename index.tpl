<div>my_var: {my_var}</div>
<div>bool: {bool_var}</div>
{if bool_var}
    <div>True</div>
    <div>{var1} > {var2}</div>
    {if var1 > var2}
        <div>True</div>
    {else}
        <div>False</div>
    {/if}
{else}
    <div>False</div>
    <div>{var1} > {var2}</div>
    <div>
        {if var1 > var2}
            <div>True</div>
        {else}
            <div>False</div>
        {/if}
    </div>
{/if}
